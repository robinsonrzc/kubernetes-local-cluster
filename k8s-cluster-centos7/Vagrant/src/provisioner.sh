#!/usr/bin/env bash

## Allow ssh login without pass
mkdir /root/.ssh
chown -R root:root /root/.ssh
chmod 700 /root/.ssh
touch /root/.ssh/authorized_keys
chmod 400 /root/.ssh/authorized_keys

