echo -e "\n ********** Configuring Kubernetes Cluster **********\n"
cd Ansible
./provision.sh
cd ../src
echo -e "\n ********** Starting Kubernetes Deployment **********\n"
scp deploy-test.yaml k8s@172.0.0.100:/home/k8s
ssh k8s@172.0.0.100 "kubectl apply -f deploy-test.yaml"
echo -e "\n\n"


while true
do
  STATUS=$(curl -s -o /dev/null -w '%{http_code}' 172.0.0.101:30001)
  STATUS2=$(curl -s -o /dev/null -w '%{http_code}' 172.0.0.102:30001)
  if [ $STATUS -eq 200 ]; then
    echo -e "\n ********** Site is alive!!! Open 172.0.0.101:30001 **********\n"
    echo -e "\n ********** ENJOY !!! **********\n"
    exit 0
  fi
  if [ $STATUS2 -eq 200 ]; then
    echo -e "\n ********** Site is alive!!! Open 172.0.0.102:30001 **********\n"
    echo -e "\n ********** ENJOY !!! **********\n"
    exit 0
  fi
  echo "Waiting for site online..."
  sleep 10
done
