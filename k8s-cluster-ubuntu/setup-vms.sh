#!/usr/bin/bash

echo -e "\n ********** Starting Virtual Machines **********\n"
cd Vagrant
vagrant up

cat << MSG


**********  Master and nodes are UP and Running  **********
            Plese run in your host the following to ensure Ansible and nodes can connect:
            ssh root@172.0.0.100
            ssh root@172.0.0.101
            ssh root@172.0.0.102

            Then run ./setup-k8s.sh
MSG
